# ISTN 코딩테스트 설명

## 정답 작성 예시

아래의 문제는 예시 문제입니다.

### <mark>A + B</mark>

### 문제

> 두 정수 A와 B를 입력받은 다음, A+B를 출력하는 함수 **addNum** 를 작성하세요.
>
> **※ ECMAScript 버전 제한 없음**
> **※ 함수명(addNum) 임의변경 금지 **

### 입력

> A와 B가 주어진다. (0 < A, B < 10)

### 출력

> Console.log 를 이용해 A+B를 출력한다.

| <mark>입력예시</mark> | <mark>출력예시</mark> |
| --------------------- | -------- |
| A = 10;<br />B = 20; | 30 |


### 코드입력
```html
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <script>
    // 코드 입력 부분
    var A = 10;
    var B = 20;

    function addNum(a, b) {
      console.log(a+b);
      return;
    }

    addNum(A, B);
  </script>
</body>

</html>
```

### git push

> 작성을 마친 후 git을 통해 gitlab에 프로그램을 commit후 push 합니다.
>
> <mark>주의사항</mark> 시험시간 종료 이후 commit을 한 경우 0점 처리됩니다.

------

# ISTN 코딩테스트 연습 문제 (v1.0.0)

## 연습문제1.

### <mark>수 비교하기</mark> 

### 문제

> A회사는 직원들(users)의 인사평가를 실시해 숫자로된 인사평가 점수를 가지고 있습니다. 
> 인사담당자는 이 점수를 알파벳으로 된 등급(A~F)으로 보고 싶어합니다.
>
> 90 ~ 100점은 A, 80 ~ 89점은 B, 70 ~ 79점은 C, 60 ~ 69점은 D, 나머지 점수는 F를 출력하는 함수 **displayUsersGrade** 를 작성하세요.
>
> **※ ECMAScript 버전 제한 없음**
> **※ 함수명(displayUsersGrade) 임의변경 금지 **
> 


### 입력

> 직원들의 인사평가 결과  users 가 주어진다.
>
> ```json
> var users = [
>     {
>         name: '하현우',
>         score: 90,
>         grade: null
>     },
>     {
>         name: '규현',
>         score: 85,
>         grade: null
>     },
>     {
>         name: '청하',
>         score: 76,
>         grade: null
>     },
>     {
>         name: '크러쉬',
>         score: 59,
>         grade: null
>     }
> ]
> ```
>

### 출력

> Console.log() 를 이용해 users 를 출력한다.
>
> <mark>예시</mark>
>
> ```javascript
> console.log(JSON.stringify(users, 2, 2))
> ```
>

<mark>출력예시</mark> 
```json
[
    {
        name: '하현우',
        score: 90,
        grade: 'A'
    },
    {
        name: '규현',
        score: 85,
        grade: 'B'
    },
    {
        name: '청하',
        score: 76,
        grade: 'C'
    },
    {
        name: '크러쉬',
        score: 59,
        grade: 'F'
    }
]
```

### 코드입력

```html
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <script>
	var users = [
        {
            name: '하현우',
            score: 90,
            grade: null
        },
        {
            name: '규현',
            score: 85,
            grade: null
        },
        {
            name: '청하',
            score: 76,
            grade: null
        },
        {
            name: '크러쉬',
            score: 59,
            grade: null
        }
  	]
    // 코드 입력 부분
  </script>
</body>

</html>
```

------


## 연습문제2.

### <mark>영수증 검증</mark> 

### 문제

> A회사의 물류담당자는 지난 주에 물건 구매를 위해 판매업체를 방문했습니다. 판매업체에서 물건 구매 후에 확인하니 영수증이 이상하게 높은 금액이 나왔다. 물류담당자는 영수증을 보면서 정확하게 계산된 것이 맞는지 확인해보려 합니다.
>
> 영수증에 적힌,
>
> - 구매한 각 물건의 가격과 개수
> - 구매한 물건들의 총 금액
>
> 을 보고, 구매한 물건의 가격과 개수로 계산한 총 금액이 영수증에 적힌 총 금액과 일치하는지 검사하는 함수인 **checkReceipt** 를 작성하세요.
>
> **※ ECMAScript 버전 제한 없음**
> **※ 함수명(checkReceipt) 임의변경 금지 **


### 입력

> 영수증에 적힌 총 금액 `total` 이 주어진다.
>
> 구매한 물품에 대한 물건명 가격 수량 데이터인  `products` 가 주어진다
>
> ```json
> var total = 295500;
> var products = [
>     {
>         name: '더블에이 A4 복사용지(A4용지) 75g 1000매',
>         price: 13200,
>         quantity: 5
>     },
>     {
>         name: '천연펄프 점보롤 300m',
>         price: 43900,
>         quantity: 5
>     },
>     {
>         name: '물티슈 100매',
>         price: 1290,
>         quantity: 20
>     }
> ]
> ```
>
> 

### 출력

> Console.log() 를 이용해 검증결과를 출력한다.
>
> <mark>예시</mark>
>
> ```javascript
> // 금액이 일치
> YES
> // 금액 불일치
> NO
> ```
>

<mark>출력예시</mark> 
```json
NO
```

### 코드입력

```html
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <script>
	var total = 295500;
    var products = [
        {
            name: '더블에이 A4 복사용지(A4용지) 75g 1000매',
            price: 13200,
            quantity: 5
        },
        {
            name: '천연펄프 점보롤 300m',
            price: 43900,
            quantity: 5
        },
        {
            name: '물티슈 100매',
            price: 1290,
            quantity: 20
        }
    ];
    // 코드 입력 부분
  </script>
</body>

</html>
```

------


## 연습문제3.

### <mark>로그인</mark> 

### 문제

> A회사의 업무담당자는 업무를 하기 위해 로그인을 하려고 합니다. 
>
> 담당자가 입력한 아이디와 패스워드가 담긴 배열 `login_info` 와 회원들의 정보가 담긴 JSON구조의 `db`가 주어질때, 
>
> 다음과 같이 로그인 성공, 실패에 따른 메세지를 return하는 함수 **loginUser**를 작성하세요.
>
> - 아이디와 비밀번호가 모두 일치하는 회원정보가 있으면 "login"을 return
> - 로그인이 실패했을 때 아이디가 일치하는 회원이 없다면 "fail"을, 아이디가 일치하지만 비밀번호가 일치하지 않는다면 "wrong pw"를 return
>
> **※ ECMAScript 버전 제한 없음**
> **※ 함수명(loginUser) 임의변경 금지 **

### 입력

> - 회원들의 아이디는 문자열입니다.
> - 회원들의 아이디는 알파벳 문자와 숫자로 이루어져 있습니다.
> - 회원들의 패스워드는 숫자로 구성된 문자열입니다.
> - 회원들의 비밀번호는 같을 수 있지만, 아이디는 같을 수 없습니다.
>
> 담당자가 입력한 아이디와 패스워드가 담긴 배열 `login_info` 와 회원들의 정보가 담긴 JSON구조의 `db`가 주어집니다.
>
> ```json
> var login_info = [
>     {
>         id: "user0001",
>         pw: "9876",
>         login: null
> 	},
>     {
>         id: "user0002",
>         pw: "9876",
>         login: null
> 	},
>     {
>         id: "user0003",
>         pw: "5555",
>         login: null
> 	},
> ];
> 
> var db = [
>     {
>         id: "user0001",
>         pw: "9876"
>     },
>     {
>         id: "user0002",
>         pw: "4321"
>     },
>     {
>         id: "user0004",
>         pw: "5678"
>     }
> ]
> ```
>


### 출력

> Console.log() 를 이용해 검증결과를 출력한다.
>
> <mark>예시</mark>
>
> ```javascript
> console.log(JSON.stringify(login_info, 2, 2))
> ```
> 

<mark>출력예시</mark> 
```json
[
    {
        id: "user0001",
        pw: "9876",
        login: "login"
	},
    {
        id: "user0002",
        pw: "9876",
        login: "wrong_pw"
	},
    {
        id: "user0003",
        pw: "5555",
        login: "fail"
	},
];
```

### 코드입력

```html
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <script>
	var login_info = [
        {
            id: "user0001",
            pw: "9876",
            login: null
        },
        {
            id: "user0002",
            pw: "9876",
            login: null
        },
        {
            id: "user0003",
            pw: "5555",
            login: null
        },
    ];

    var db = [
        {
            id: "user0001",
            pw: "9876"
        },
        {
            id: "user0002",
            pw: "4321"
        },
        {
            id: "user0004",
            pw: "5678"
        }
    ]
    // 코드 입력 부분
  </script>
</body>

</html>
```
